# Overview

Avantra is an industry leading AIOps platform for SAP Operations: helping companies transform into a self-healing enterprise. 
Avantra helps SAP customers to improve business experience, performance and compliance, focus valuable resources on digital transformation and reduce operational cost.
 Avantra delivers a unified AIOps platform, whether on-premises, in the cloud, SaaS or hybrid.

For more information on Avantra, see the [Avantra official website](https://www.avantra.com/).

## Design

![Architecture diagram](resources/avantra-architecture.png)

This application offers a stateful installation of Avantra on a Kubernetes cluster.

The application uses StatefulSet for Avantra UI, Avantra Master and Avantra Master Worker.
# Prerequisites

- [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
- [PostgreSQL](https://www.postgresql.org/) (Version 9.6, Version 10, Version 11) Version 12 is currently not supported
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)


# Installation

Get up and running with just a few clicks! Install this Avantra app to a Google Kubernetes Engine cluster by following the
[on-screen instructions](https://console.cloud.google.com/marketplace/details/avantra-public/avantra) within Google Cloud Marketpalce.

## Create Kubernetes Cluster

To use Avantra in GCP, we need a Kubernetes Cluster. If a cluster already exists, you can skip the following steps.
If no cluster exists yet, the following steps explain how to create a cluster for Avantra in GCP. We will create a cluster that is exclusively used for Avantra, if you want to use more applications in Kubernetes you have to configure a bigger cluster.

In the [GCP Console](https://console.cloud.google.com) under the menu item Kubernetes Engine > Cluster we can create a new cluster.


![Architecture diagram](resources/setup/kubernetes/Kubernetes_2.png)

![Architecture diagram](resources/setup/kubernetes/Kubernetes_3.png)


First our cluster gets a name and we define the zone or region where the cluster is created.


![Architecture diagram](resources/setup/kubernetes/Kubernetes_4.png)


The settings under "default-pool" can all remain on the default values. If we need the cluster for other applications we can configure more nodes under "Size".


![Architecture diagram](resources/setup/kubernetes/Kubernetes_5.png)

Under the "Nodes" menu item we define the size of our individual nodes. For a cluster exclusive for Avantra the series "E2" and there the type "e2-medium" is well suited. For a large SAP landscape, you should choose a type that offers more performance and therefore Avantra also has more power available.

![Architecture diagram](resources/setup/kubernetes/Kubernetes_6.png)

All other settings can remain at the default values and the cluster can now be created.

![Architecture diagram](resources/setup/kubernetes/Kubernetes_7.png)

## Create PostgreSQL Database

In these steps we set up a PostgreSQL database in GCP. For this we use the managed variant which offers GCP. It is also possible to install a PostgreSQL database in Kubernetes, but this is not explained here.
It is important to note that Avantra supports versions 9.6, 10 and 11 of PostgreSQL.

Under the menu item "SQL" we can create a new Cloud SQL Instance. 

![Architecture diagram](resources/setup/db/DB_1.png)

![Architecture diagram](resources/setup/db/DB_2.png)

There we choose the PostgreSQL option.

![Architecture diagram](resources/setup/db/DB_3.png)

We give our instance a name and set the password for the "postgres" user. We also decide on a region and a zone. Very important is that we configure the database version 9.6, 10 or 11, because only these versions are supported by Avantra. 

![Architecture diagram](resources/setup/db/DB_4.png)

Under Configuration options > Connectivity we select "Private IP". All other settings can be left at their default values.

![Architecture diagram](resources/setup/db/DB_5.png)

Once our database is created we can see all the important information. For all further steps it is important that we keep the IP of the database in mind.

![Architecture diagram](resources/setup/db/DB_6.png)

Under User we can create a user for Avantra. This user must be able to create databases and tables in the database. We also have to remember this information, because we will need it later.

![Architecture diagram](resources/setup/db/DB_7.png)

## Install Avantra

Now we can install Avantra in our Kubernetes cluster. To do so we search for Avantra in the GCP Marketplace or use the link. [Avantra GCP Marketplace](https://console.cloud.google.com/marketplace/details/avantra-public/avantra)

![Architecture diagram](resources/setup/avantra/Avantra_1.png)

![Architecture diagram](resources/setup/avantra/Avantra_2.png)

First we buy Avantra, there are no costs yet, but the GCP account is prepared for billing. After that we can configure Avantra.

![Architecture diagram](resources/setup/avantra/Avantra_3.png)

We configure the Kubernetes cluster and the namespace where we want to install Avantra. Assign an application name under which we want to install Avantra and specify the database information of our database. The default port of PostgreSQl is "5432". We can accept all other default values. If we want to have a larger SAP landscape monitored by Avantra, we should provide the individual container with more CPU and memory. It is important that we also have nodes in our cluster that can fulfill the CPU and memory requirements.

Then we can install Avantra.

![Architecture diagram](resources/setup/avantra/Avantra_4.png)

Under the menu item Applications we can find our Avantra installation. There we also find a link to the UI of Avantra. The Avantra Master Container is shown with warnings at the beginning, this is because we still have to configure Avantra ourselves. Therefore we click on the link for the Avantra UI.

![Architecture diagram](resources/setup/avantra/Avantra_5.png)

![Architecture diagram](resources/setup/avantra/Avantra_6.png)

![Architecture diagram](resources/setup/avantra/Avantra_7.png)

We now follow the steps in the UI and configure Avantra.

![Architecture diagram](resources/setup/avantra/Avantra_8.png)

![Architecture diagram](resources/setup/avantra/Avantra_9.png)

![Architecture diagram](resources/setup/avantra/Avantra_10.png)

Once the configuration is complete, we can log into Avantra with the newly created "root" user.

![Architecture diagram](resources/setup/avantra/Avantra_11.png)

![Architecture diagram](resources/setup/avantra/Avantra_12.png)

Under the menu item About we have to configure the Master and UI Host as last step. We can find this information on the Avantra application page in the GCP console. There we find the Master and the UI Service under Components. These two names are also the host names which we have to configure in Avantra.

![Architecture diagram](resources/setup/avantra/Avantra_13.png)

![Architecture diagram](resources/setup/avantra/Avantra_14.png)

With the completed configuration of Avantra, the Avantra Master Container now also goes into the Ready status.

![Architecture diagram](resources/setup/avantra/Avantra_15.png)

# Scaling Avantra

By default, the Avantra application is deployed using 1 replicas.

> **NOTE:** Scaling Avantra is not supported.

# Upgrading the app

The Avantra StatefulSet's are not configured to roll out updates automatically. First, patch
the StatefulSet's with a new image references:

```shell
kubectl set image statefulset ${APP_INSTANCE_NAME}-ui --namespace ${NAMESPACE} \
  "ui=[NEW_IMAGE_REFERENCE]"
```

```shell
kubectl set image statefulset ${APP_INSTANCE_NAME}-master --namespace ${NAMESPACE} \
  "ui=[NEW_IMAGE_REFERENCE]"
```

```shell
kubectl set image statefulset ${APP_INSTANCE_NAME}-worker --namespace ${NAMESPACE} \
  "ui=[NEW_IMAGE_REFERENCE]"
```

In this case, `[NEW_IMAGE_REFERENCE]` should be the reference to the new Docker image that you want to use.


Then Kubernetes can roll out a new version of Avantra.

# Uninstall the Application

## Using the Google Cloud Platform Console

1. In the GCP Console, open [Kubernetes Applications](https://console.cloud.google.com/kubernetes/application).

1. From the list of applications, click **Avantra**.

1. On the Application Details page, click **Delete**.

## Delete the persistent volumes of your installation

By design, the removal of StatefulSets in Kubernetes does not remove
PersistentVolumeClaims that were attached to their Pods. This prevents your
installations from accidentally deleting stateful data.

To remove the PersistentVolumeClaims with their attached persistent disks, run
the following `kubectl` commands:

```shell
kubectl delete persistentvolumeclaims \
  --namespace ${NAMESPACE} ui-slr-data-ui-0

kubectl delete persistentvolumeclaims \
  --namespace ${NAMESPACE} ui-index-data-ui-0

kubectl delete persistentvolumeclaims \
  --namespace ${NAMESPACE} master-index-data-master-0
```

## Delete the GKE cluster

Optionally, if you don't need the deployed application or the GKE cluster,
you can use this command to delete the cluster:

```shell
gcloud container clusters delete "${CLUSTER}" --zone "${ZONE}"
```
